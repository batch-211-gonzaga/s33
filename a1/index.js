const URL = 'http://jsonplaceholder.typicode.com/todos';

fetch(URL + '/1')
.then((response) => response.json())
.then((json) => console.log(json));


fetch(URL)
.then((response) => response.json())
.then((json) => {
  const titles = json.map(todo => todo.title);
  console.log(titles);
});

fetch(URL + '/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// Create a todo item using POST
fetch(URL, {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    title: 'Created To Do List Item',
    completed: false,
    userId: 1
  })
})
.then((response) => response.json())
.then((json) => console.log(json))

// Update using PUT
fetch(URL + '/1', {
  method: 'PUT',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    title: 'Updated To Do List Item',
    description: 'To update the my to do list with a different data structure',
    status: 'Pending',
    dateCompleted: 'Pending',
    userID: 1
  })
})
.then((response) => response.json())
.then((json) => console.log(json))

// Update using PATCH
fetch(URL + '/1', {
  method: 'PATCH',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    status: 'Complete',
    dateCompleted: '1/1/22'
  })
})
.then((response) => response.json())
.then((json) => console.log(json))

// Delete
fetch(URL + '/1', { method: 'DELETE' })
.then((response) => response.json())
.then((json) => console.log(json))
